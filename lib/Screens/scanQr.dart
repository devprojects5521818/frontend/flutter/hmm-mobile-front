import 'package:HmmAndroidFront/Screens/MyOrders.dart';
import 'package:HmmAndroidFront/Screens/cartpage.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  String qrCodeResult = "Not Yet Scanned";

  Future scanqr() async {
    print("Printing here");
    
    String codeSanner = await BarcodeScanner.scan(); //barcode scnner
    setState(() {
      qrCodeResult = codeSanner;
    });
  }

  Future viewmyorders() async {
    print("Printing here in View my Orders");
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("CONFIRMATION"),
        content: Text("Are You Sure you want to exit the app ?"),
        actions: <Widget>[
          FlatButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text("NO")),
          FlatButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text("YES")),
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: Color(0xFFF5F4F9),
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Text(
            "Home",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          
          brightness: Brightness.light,
          backgroundColor: Color.fromARGB(255, 255, 208, 0),
          leading: null,
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                        height: size.longestSide * 0.4,
                        width: size.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Swiper(
                            fade: 0.5,
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      height: size.shortestSide / 0.5,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 2.0,
                                          ),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10.0),
                                              topRight: Radius.circular(10.0)),
                                          image: DecorationImage(
                                              image: NetworkImage(
                                                  'https://firebasestorage.googleapis.com/v0/b/eventapp-3c489.appspot.com/o/Head_Images%2F1611492419582?alt=media&token=5856f99f-eba0-44c2-a06a-6bde198f0e37'),
                                              fit: BoxFit.cover)),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 35.0),
                                    child: Container(
                                      height: size.shortestSide * 0.18,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 2.0,
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(10.0),
                                              bottomRight:
                                                  Radius.circular(10.0))),
                                      child: Center(
                                        child: ListTile(
                                          subtitle: Text(
                                              "Item Description and Image",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15)),
                                          title: Text("Item Name",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                            pagination: new SwiperPagination(
                              alignment: Alignment.bottomCenter,
                              builder: new DotSwiperPaginationBuilder(
                                  color: Colors.grey,
                                  activeColor: Colors.black),
                            ),
                            autoplay: true,
                            itemCount: 7,
                            viewportFraction: 0.8,
                            scale: 0.9,
                          ),
                        )),
                    SizedBox(height: 5.0),
                    Container(
                      color: Colors.white,
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: Card(
                              color: Colors.black,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              elevation: 5,
                              child: GestureDetector(
                                onTap: scanqr,
                                child: Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.4,
                                    decoration: BoxDecoration(
                                        color: Colors.black,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0),
                                            topLeft: Radius.circular(10.0),
                                            topRight: Radius.circular(10.0))),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(15.0),
                                            child: Image.asset(
                                              'assets/images/scan.png',
                                              width: size.shortestSide * 0.4,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 30.0),
                                          child: Text(
                                            "Scan QR Code",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16),
                                          ),
                                        )
                                      ],
                                    )),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              elevation: 5,
                              child: GestureDetector(
                                child: Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.35,
                                    decoration: BoxDecoration(
                                        color: Colors.black,
                                        borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0),
                                            topLeft: Radius.circular(10.0),
                                            topRight: Radius.circular(10.0))),
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(15.0),
                                            child: Image.asset(
                                              'assets/images/food.png',
                                              width: size.shortestSide * 0.3,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 30.0),
                                          child: Text(
                                            "View My Orders",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16),
                                          ),
                                        )
                                      ],
                                    )),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ViewMyOrders()));
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      width: size.shortestSide,
                      height: size.shortestSide * 0.20,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0),
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0))),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: "Search for Restaurants",
                              hintStyle: TextStyle(color: Colors.black),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              prefixIcon: Padding(
                                padding: EdgeInsets.all(0.0),
                                child: Icon(
                                  Icons.search,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //its quite simple as that you can use try and catch staatements too for platform exception

}
