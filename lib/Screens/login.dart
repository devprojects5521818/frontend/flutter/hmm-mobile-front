import 'package:HmmAndroidFront/Screens/sendotp.dart';
import 'package:HmmAndroidFront/Screens/userbottomnavbar.dart';

import 'package:HmmAndroidFront/components/background.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isphonenovalidated = false;
  bool _ispassvalidated = false;
  String username = "Rahul";

  GlobalKey<FormState> form = GlobalKey<FormState>();
  GlobalKey<FormState> passform = GlobalKey<FormState>();

  TextEditingController phonenumbercontroller = new TextEditingController();
  TextEditingController passwordcontroller = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    _isphonenovalidated = false;
    _ispassvalidated = false;
    super.initState();
  }

  void navigatetohome() {
    Get.offAll(() => UserNavBar());
    Get.snackbar("Logged In Successfully", "Welcome" + " " + username,
        colorText: Color.fromARGB(255, 255, 208, 0),
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.black);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    passwordcontroller.clear();
    phonenumbercontroller.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Background(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Text(
                    "Login",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 36),
                    textAlign: TextAlign.left,
                  ),
                ),
                SizedBox(height: size.height * 0.04),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child: makePhoneNumberInput(label: "Phone Number"),
                ),
                SizedBox(height: 0.1),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child:
                      makePasswordInput(label: "Password", obscureText: true),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.symmetric(
                    horizontal: 40,
                  ),
                  child: Text(
                    "Forgot your password?",
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                ),
                SizedBox(height: size.height * 0.05),
                Container(
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.symmetric(
                    horizontal: 30,
                    vertical: 10,
                  ),
                  child: RaisedButton(
                    onPressed: _isphonenovalidated && _ispassvalidated
                        ? () => navigatetohome()
                        : null,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    textColor: Colors.white,
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      alignment: Alignment.center,
                      height: 50.0,
                      width: size.width * 0.5,
                      decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: _isphonenovalidated && _ispassvalidated
                            ? Colors.black
                            : Colors.grey[200],
                      ),
                      padding: const EdgeInsets.all(0),
                      child: Text(
                        "Login",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: _isphonenovalidated && _ispassvalidated
                                ? Color.fromARGB(255, 255, 208, 0)
                                : Colors.grey,
                            fontSize: 20),
                      ),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SendOTP()))
                    },
                    child: Text(
                      "Don't Have an Account? Sign up",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget makePhoneNumberInput({label, obscureText = false}) {
    return Form(
      onChanged: () {
        setState(() {
          _isphonenovalidated = form.currentState.validate();
        });
      },
      autovalidate: true,
      //autovalidateMode: AutovalidateMode.always,
      key: form,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: phonenumbercontroller,
            keyboardType: TextInputType.number,
            validator: MultiValidator([
              RequiredValidator(errorText: "Required ***"),
              MinLengthValidator(10,
                  errorText: 'Enter 10 Digit Mobile Number ***'),
              MaxLengthValidator(10,
                  errorText: 'Mobile Number cannot exceed 10 digits ***'),
            ]),
            obscureText: obscureText,
            maxLength: 10,
            decoration: InputDecoration(
              prefixText: '+91',
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  'assets/images/flag.png',
                  height: 0.5,
                  width: 0.5,
                ),
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
        ],
      ),
    );
  }

  Widget makePasswordInput({label, obscureText = false}) {
    return Form(
      autovalidate: true,
      onChanged: () {
        setState(() {
          _ispassvalidated = passform.currentState.validate();
        });
      },
      key: passform,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: passwordcontroller,
            validator: MultiValidator([
              RequiredValidator(errorText: 'Password is required'),
              MinLengthValidator(8,
                  errorText: 'Password must be at least 8 digits long'),
              PatternValidator(r'(?=.*?[#?!@$%^&*-])',
                  errorText:
                      'Password must have at least one special character')
            ]),
            obscureText: obscureText,
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.admin_panel_settings,
                color: Colors.black,
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
