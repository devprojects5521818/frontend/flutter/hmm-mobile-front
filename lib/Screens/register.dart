import 'package:HmmAndroidFront/Screens/login.dart';
import 'package:HmmAndroidFront/animation/FadeAnimation.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  GlobalKey<FormState> firstnameform = GlobalKey<FormState>();
  GlobalKey<FormState> lastnameform = GlobalKey<FormState>();
  GlobalKey<FormState> emailform = GlobalKey<FormState>();

  bool isFirstnameValidated = false;
  bool isLastnameValidated = false;

  void navigatetologin() {
    if (this.mounted) {
      Get.offAll(() => LoginPage());
      Get.snackbar("Registered Successfully", "Please Login",
          colorText: Color.fromARGB(255, 255, 208, 0),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.black);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios, size: 20, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 40),
          height: MediaQuery.of(context).size.height - 28,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  FadeAnimation(
                      1,
                      Text(
                        "Sign up",
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold),
                      )),
                  SizedBox(
                    height: 20,
                  ),
                  FadeAnimation(
                      1.2,
                      Text(
                        "Create an account, It's free",
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      )),
                ],
              ),
              Column(
                children: <Widget>[
                  FadeAnimation(1.2,
                      makeFirstNameInput(label: "Enter Your First Name * ")),
                  FadeAnimation(
                      1.3, makeLastNameInput(label: "Enter Your Last Name * ")),
                  FadeAnimation(
                      1.4,
                      makeEmailInput(
                          label: "Enter Your Email Address (optional) ")),
                ],
              ),
              FadeAnimation(
                  1.5,
                  Container(
                    padding: EdgeInsets.only(top: 3, left: 3),
                    child: MaterialButton(
                      minWidth: double.infinity,
                      color: isFirstnameValidated && isLastnameValidated
                          ? Colors.black
                          : Colors.grey[200],
                      height: 60,
                      onPressed: isFirstnameValidated && isLastnameValidated
                          ? () => navigatetologin()
                          : null,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        "Proceed",
                        style: TextStyle(
                            color: isFirstnameValidated && isLastnameValidated
                                ? Color.fromARGB(255, 255, 208, 0)
                                : Colors.grey,
                            fontWeight: FontWeight.w600,
                            fontSize: 18),
                      ),
                    ),
                  )),
              FadeAnimation(
                  1.6,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Already have an account?"),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()));
                        },
                        child: Text(
                          " Login",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 18),
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget makeFirstNameInput({label, obscureText = false}) {
    return Form(
      autovalidate: true,
      key: firstnameform,
      onChanged: () {
        setState(() {
          isFirstnameValidated = firstnameform.currentState.validate();
        });
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            validator: MultiValidator([
              MinLengthValidator(4,
                  errorText: "Minimum Length of Name Should be 4 ***"),
              MaxLengthValidator(10,
                  errorText:
                      "Maximum Length of Name Should not be more than 10 ***"),
              RequiredValidator(errorText: "Required ***")
            ]),
            obscureText: obscureText,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  Widget makeLastNameInput({label, obscureText = false}) {
    return Form(
      autovalidate: true,
      key: lastnameform,
      onChanged: () {
        setState(() {
          isLastnameValidated = lastnameform.currentState.validate();
        });
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            validator: MultiValidator([
              MinLengthValidator(4,
                  errorText: "Minimum Length of Name Should be 4 ***"),
              MaxLengthValidator(10,
                  errorText: "Max Length should not be more than 10 ***"),
              RequiredValidator(errorText: "Required ***")
            ]),
            obscureText: obscureText,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  Widget makeEmailInput({label, obscureText = false}) {
    return Form(
      autovalidate: true,
      key: emailform,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            validator: MultiValidator([
              EmailValidator(errorText: "Enter Valid Email ***"),
            ]),
            obscureText: obscureText,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
