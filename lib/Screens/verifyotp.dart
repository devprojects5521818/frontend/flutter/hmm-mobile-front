import 'package:HmmAndroidFront/Screens/register.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:otp_count_down/otp_count_down.dart';

import 'register.dart';
import 'register.dart';

final inputBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(8.0),
  borderSide: BorderSide(color: Colors.grey.shade400),
);

final inputDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 16.0),
  border: inputBorder,
  focusedBorder: inputBorder,
  enabledBorder: inputBorder,
);

class VerifyOTP extends StatefulWidget {
  final String phnumber;
  const VerifyOTP({Key key, @required this.phnumber}) : super(key: key);
  @override
  _VerifyOTPState createState() => _VerifyOTPState();
}

class _VerifyOTPState extends State<VerifyOTP> {
  String _countDown = " ";
  OTPCountDown _otpCountDown;
  final int _otpTimeInMS = 1000 * 1 * 40;
  bool makeresendotpbuttonenabled = false;
  bool makeVerifybuttonenabled = false;

  final pinStyle = TextStyle(fontSize: 32, fontWeight: FontWeight.bold);

  TextEditingController controller1 = new TextEditingController();

  GlobalKey<FormState> otpformkey = new GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    _startCountDown();
    makeresendotpbuttonenabled = false;
    makeVerifybuttonenabled = false;
    super.initState();
  }

  void _startCountDown() {
    _otpCountDown = OTPCountDown.startOTPTimer(
      timeInMS: _otpTimeInMS,
      currentCountDown: (String countDown) {
        _countDown = countDown;
        setState(() {});
      },
      onFinish: () {
        setState(() {
          makeresendotpbuttonenabled = true;
        });
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  final otpValidator = MultiValidator([
    RequiredValidator(errorText: ' '),
  ]);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.black,
          ),
        ),
        title: Text(
          "Verify phone",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        textTheme: Theme.of(context).textTheme,
      ),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Container(
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 14),
                    child: Text(
                      "Code is sent to " + widget.phnumber,
                      style: TextStyle(
                        fontSize: 22,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  Text(
                    "Please enter the 6-digit OTP",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18.0),
                  ),
                  const SizedBox(height: 20.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child:
                        makeInput(label: "Enter Your OTP", obscureText: true),
                  ),
                  const SizedBox(height: 20.0),
                  Text(
                    "Expiring in " + _countDown,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0, color: Colors.black),
                  ),
                  const SizedBox(height: 10.0),
                  makeresendotpbuttonenabled
                      ? RaisedButton(
                          onPressed: () {},
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            alignment: Alignment.center,
                            height: 50.0,
                            width: size.width * 0.5,
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.black,
                            ),
                            padding: const EdgeInsets.all(0),
                            child: Text(
                              "Resend Otp",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromARGB(255, 255, 208, 0),
                                  fontSize: 20),
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.13,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(25),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: MaterialButton(
                      minWidth: double.infinity,
                      color: makeVerifybuttonenabled ? Colors.black : Colors.grey[200],
                      height: 60,
                      onPressed: makeVerifybuttonenabled ? () => Get.to(() => SignUpPage()) : null,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        "Verify and Create Account",
                        style: TextStyle(
                            color: makeVerifybuttonenabled ? Color.fromARGB(255, 255, 208, 0) : Colors.grey,
                            fontWeight: FontWeight.w600,
                            fontSize: 18),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }

  Widget makeInput({label, obscureText = false}) {
    return Form(
      key: otpformkey,
      onChanged: () {
        setState(() {
          makeVerifybuttonenabled = otpformkey.currentState.validate();
        });
      },
      autovalidate: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: controller1,
            validator: MultiValidator([
              RequiredValidator(errorText: "Required ***"),
              MinLengthValidator(6,
                  errorText: 'Enter your 6 digit otp ***'),
            ]),
            keyboardType: TextInputType.number,
            obscureText: obscureText,
            maxLength: 6,
            decoration: InputDecoration(
             
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[400])),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[400])),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
