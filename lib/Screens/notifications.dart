import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 5.0,
        automaticallyImplyLeading: false,
        title: Text(
          "Notifications",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Color.fromARGB(255, 255, 208, 0),
        leading: null,
      ),
      body: ListView.builder(
          itemCount: 10,
          itemBuilder: (BuildContext context, index) {
            return Dismissible(
                key: ValueKey(index),
                onDismissed: (direction) {},
                direction: DismissDirection.startToEnd,
                background: Container(
                  alignment: Alignment.centerLeft,
                  color: Colors.red,
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Row(
                      children: [
                        Icon(Icons.delete_forever_outlined),
                        Text("Delete")
                      ],
                    ),
                  ),
                ),
                child: dialogContent(context, "Some Title",
                    "Stack widget stacks its children on top of each "));
          }),
    );
  }

  Widget dialogContent(BuildContext context, String title, String message) {
    return  Container(
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
            itemCount: 15,
            itemBuilder: (context, index) {
              return Card(
                margin: EdgeInsets.only(left: 0.0, right: 0.0),
                child: Container(
                  child:Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        height: MediaQuery.of(context).size.shortestSide * 0.4,
        width: MediaQuery.of(context).size.shortestSide,
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.shortestSide,
              height: MediaQuery.of(context).size.shortestSide * 0.4,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                    width: 1.0,
                  ),
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ]),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 18.0, top: 8),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FittedBox(
                            child: new Text(
                              title,
                              style: TextStyle(
                                  fontSize: 18.0, color: Colors.black),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(message,
                              style: TextStyle(
                                  fontSize: 15.0, color: Colors.black)),
                          SizedBox(
                            height: 35.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 40.0),
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Text(DateTime.now().toIso8601String(),
                                  style: TextStyle(
                                      fontSize: 15.0, color: Colors.black)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    )));}));
  }
}
