import 'package:flutter/material.dart';

class ViewMyOrders extends StatefulWidget {
  @override
  _ViewMyOrdersState createState() => _ViewMyOrdersState();
}

class _ViewMyOrdersState extends State<ViewMyOrders> {
  int count = 0;
  var date = DateTime.now();

  void _showBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Container(
            color: Color.fromRGBO(0, 0, 0, 0.001),
            child: GestureDetector(
              onTap: () {},
              child: DraggableScrollableSheet(
                initialChildSize: 0.6,
                minChildSize: 0.2,
                maxChildSize: 0.75,
                builder: (_, controller) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(25.0),
                        topRight: const Radius.circular(25.0),
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Icon(
                          Icons.remove,
                          color: Colors.grey[600],
                        ),
                        Expanded(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListView.builder(
                              itemCount: 5,
                              itemBuilder: (context, index) {
                                return _buildFoodItem(
                                  'assets/images/verify.png',
                                  'Hotel Gurudev',
                                  'Indian, Chinese',
                                  "Khadkpada, Kalyan",
                                );
                              }),
                        )),
                        SizedBox(height: 10.0),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 10.0, left: 20.0),
                              child: Column(
                                children: [
                                  Text("No. of Items : "),
                                  SizedBox(height: 10.0),
                                  Text("Total Price : "),
                                ],
                              ),
                            )),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20.0,bottom: 20.0),
                            child: RaisedButton(
                              onPressed: () {},
                              child: Text(
                                "Place Order",
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Text(
            "My Orders",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          //toolbarHeight: 0.5,
          brightness: Brightness.light,
          backgroundColor: Color.fromARGB(255, 255, 208, 0),
          leading: null,
          bottom: TabBar(
            tabs: [
              Tab(
                  child: Text("Live Orders",
                      style: TextStyle(color: Colors.black))),
              Tab(
                  child: Text("Previous Orders",
                      style: TextStyle(color: Colors.black))),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            liveOrders(),
            previousOrders(),
          ],
        ),
      ),
    );
  }

  Widget liveOrders() {
    if (count > 0) {
      return Center(child: Text("Live Orders Data"));
    } else {
      return noLiveOrderOrderFound();
    }
  }

  Widget previousOrders() {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: ListView.builder(
          itemCount: 3,
          itemBuilder: (BuildContext context, index) {
            return orderContent(context, "123123123", "Hotel Gopal Kishna");
          }),
    );
  }

  Widget orderContent(BuildContext context, String id, String hotelName) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: size.shortestSide * 1.2,
          child: Card(
            color: Colors.grey[200],
            elevation: 0.5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
              side: new BorderSide(color: Colors.black, width: 2.5),
            ),
            child: Container(
              child: Padding(
                padding: EdgeInsets.all(1.0),
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.black,
                          border: Border.all(
                            color: Colors.black,
                          ),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 15.0, top: 15),
                                child: Text(
                                  "id : ",
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    color: Color.fromARGB(255, 255, 208, 0),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 15),
                                child: Text(
                                  id,
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    color: Color.fromARGB(255, 255, 208, 0),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15.0, top: 8),
                            child: Text(
                              hotelName,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Color.fromARGB(255, 255, 208, 0),
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15.0, top: 8),
                            child: Text(
                              DateTime.fromMicrosecondsSinceEpoch(
                                      date.microsecondsSinceEpoch)
                                  .toString(),
                              style: TextStyle(
                                fontSize: 17,
                                color: Color.fromARGB(255, 255, 208, 0),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: Expanded(
                        child: Container(
                          child: Padding(
                              padding: EdgeInsets.only(left: 15, right: 15),
                              child: Container(
                                height: MediaQuery.of(context).size.height,
                                child: ListView.builder(
                                    itemCount: 5,
                                    itemBuilder: (context, index) {
                                      return _buildFoodItem(
                                          'assets/images/plate1.png',
                                          'Triple Schzewan Rice',
                                          'Price: \$24.00',
                                          "Quantity: 3");
                                    }),
                              )),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 20.0, bottom: 10.0, top: 2.0),
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: RaisedButton(
                          onPressed: () {
                            _showBottomSheet(context);
                          },
                          color: Color.fromARGB(255, 255, 208, 0),
                          child: Text("View Details"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFoodItem(
      String imgPath, String foodName, String price, String quantity) {
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Colors.black,
                ),
              ),
              child: Padding(
                  padding: EdgeInsets.all(0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                          child: Row(children: [
                        Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  right: BorderSide(color: Colors.black))),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Image(
                                image: AssetImage(imgPath),
                                fit: BoxFit.cover,
                                height: 75.0,
                                width: 75.0),
                          ),
                        ),
                        SizedBox(width: 30.0),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(foodName,
                                  style: TextStyle(
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 5.0),
                              Text(price,
                                  style: TextStyle(
                                      fontSize: 15.0, color: Colors.black)),
                              SizedBox(height: 5.0),
                              Text(quantity,
                                  style: TextStyle(
                                      fontSize: 15.0, color: Colors.black))
                            ])
                      ])),
                    ],
                  )))),
    );
  }

  Widget noLiveOrderOrderFound() {
    return Container(
      color: Colors.grey[200],
      height: MediaQuery.of(context).size.longestSide * 0.5,
      width: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 100.0),
          child: Column(
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Image.asset('assets/images/noorder1.png'),
                ),
              ),
              Text(
                "No Live Orders Found",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Please Make New Orders",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15.0,
                    fontStyle: FontStyle.normal),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
