import 'package:flutter/material.dart';

class HotelList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return hotelList(context);
  }

  Widget hotelList(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Text(
            "Search Hotels",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Color.fromARGB(255, 255, 208, 0),
          leading: null,
        ),
        body: Container(
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                    height: size.shortestSide * 8.2,
                    child: Container(
                        child: Padding(
                            padding: EdgeInsets.all(0.0),
                            child: Container(
                                height: MediaQuery.of(context).size.height,
                                width: MediaQuery.of(context).size.shortestSide,
                                child: ListView.builder(
                                    itemCount: 15,
                                    itemBuilder: (context, index) {
                                      return _buildFoodItem(
                                          'assets/images/hotel.png',
                                          'Hotel Gurudev',
                                          'Indian, Chinese',
                                          "Khadkpada, Kalyan",
                                          '3.8',
                                          context);
                                    }))))))));
  }

  Widget _buildFoodItem(String imgPath, String foodName, String speciality,
      String address, String rating, BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Container(
              width: MediaQuery.of(context).size.shortestSide,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Colors.black,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                  padding: EdgeInsets.all(2.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Image(
                                image: AssetImage(imgPath),
                                fit: BoxFit.fitHeight,
                                height:
                                    MediaQuery.of(context).size.shortestSide *
                                        0.5,
                                width:
                                    MediaQuery.of(context).size.shortestSide *
                                        0.335,
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(foodName,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(height: 10.0),
                                  Text(speciality,
                                      style: TextStyle(
                                          fontSize: 16.0, color: Colors.black)),
                                  SizedBox(height: 10.0),
                                  Text(address,
                                      style: TextStyle(
                                          fontSize: 16.0, color: Colors.black)),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Container(
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.star,
                                          color: Colors.yellow[600],
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        Text(rating,
                                            style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.black,
                                            )),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(width: 90),
                                      Container(
                                        child: RaisedButton(
                                          textColor: Colors.black,
                                          color: Colors.amber[400],
                                          onPressed: () {},
                                          child: Row(
                                            children: [
                                              new Text('Explore'),
                                              Icon(Icons.arrow_right)
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ]),
                            // ),
                          ],
                        ),
                      ),
                    ],
                  )))),
    );
  }
}
