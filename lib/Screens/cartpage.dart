import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: true,
          centerTitle: true,
          title: Text(
            "Cart",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Color.fromARGB(255, 255, 208, 0),
          leading: IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
            Navigator.pop(context);
            
          }),
        ),
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.longestSide,
          child: Center(
            child: showEmptyCart(),
          ),
          
        ),
      ),
    );
  }

  Widget showEmptyCart() {
    return Container(
      height: MediaQuery.of(context).size.longestSide,
      width: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 150.0),
          child: Column(
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Image.asset('assets/images/emptycart.png'),
                ),
              ),
              Text(
                "Your Shopping Cart is Empty",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic
                ),
              ),
              SizedBox(height: 10.0,),
              Text(
                "Please Make New Orders",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                  fontStyle: FontStyle.normal
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}