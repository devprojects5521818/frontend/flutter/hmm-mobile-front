import 'package:HmmAndroidFront/Screens/hotelList.dart';
import 'package:HmmAndroidFront/Screens/MyOrders.dart';
import 'package:HmmAndroidFront/Screens/notifications.dart';
import 'package:HmmAndroidFront/Screens/profile.dart';
import 'package:HmmAndroidFront/Screens/register.dart';
import 'package:HmmAndroidFront/Screens/scanQr.dart';
import 'package:HmmAndroidFront/Screens/searchHotel.dart';
import 'package:flutter/material.dart';

class UserNavBar extends StatefulWidget {
  @override
  _UserNavBarState createState() => _UserNavBarState();
}

class _UserNavBarState extends State<UserNavBar> {
  int _selectedIndex = 0;

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> _buildScreens = [
      ScanPage(),
      HomeScreen(),
      ViewMyOrders(),
      CustomDialog(),
      ProfileScreen()
    ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _buildScreens.elementAt(_selectedIndex)
      ),
      bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.grey[600],
          selectedFontSize: 0,
          items: [
            BottomNavigationBarItem(
              backgroundColor: Color.fromARGB(255, 255, 208, 0),
              icon: Icon(Icons.home_outlined, size: 35),
              label: "Home",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color.fromARGB(255, 255, 208, 0),
              icon: Icon(Icons.food_bank_outlined, size: 35),
              label: "Search Restaurants",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color.fromARGB(255, 255, 208, 0),
              icon: Icon(Icons.fastfood_outlined, size: 30),
              label: "My Orders",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color.fromARGB(255, 255, 208, 0),
              icon: Icon(Icons.notifications_outlined, size: 35),
              label: "Notifications",
            ),
             BottomNavigationBarItem(
              backgroundColor: Color.fromARGB(255, 255, 208, 0),
              icon: Icon(Icons.account_circle_outlined , size: 35),
              label: "Profile",
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTap,
        ));
    
  }
}