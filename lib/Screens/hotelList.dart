import 'package:flutter/material.dart';
import 'package:HmmAndroidFront/Screens/recommendationcaroursel.dart';
import 'package:HmmAndroidFront/Screens/hotel_carousel.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 30.0),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 120.0),
              child: Text(
                'What would you like to Eat?',
                style: TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 20.0),
            Row(
              children: [
                Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Center(
                          child: Container(
                            width:
                                MediaQuery.of(context).size.shortestSide * 0.94,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              border: Border.all(
                                color: Colors.grey[400],
                                // width: 1.0,
                              ),
                            ),
                            child: TextField(
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16.0),
                                decoration: InputDecoration(
                                    hintText: "Search Foods",
                                    border: InputBorder.none,
                                    icon: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 18.0),
                                      child: Icon(Icons.search,
                                          color: Colors.black),
                                    ))),
                          ),
                        ),
                      ),
                    )),
              ],
            ),
            SizedBox(height: 20.0),
            DestinationCarousel(),
            SizedBox(height: 20.0),
            HotelCarousel(),
          ],
        ),
      ),
    );
  }
}
