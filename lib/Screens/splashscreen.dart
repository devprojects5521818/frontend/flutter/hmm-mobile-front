import 'package:HmmAndroidFront/Screens/welcome.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSplashScreen(
        splash: Image.asset('assets/images/splashchef.png'),
        nextScreen: Welcome(),
        duration: 3000,
        backgroundColor: Color.fromARGB(255, 255, 208, 0),
        splashIconSize: MediaQuery.of(context).size.shortestSide * 0.8,
        splashTransition: SplashTransition.slideTransition,
      ),
    );
  }
}
