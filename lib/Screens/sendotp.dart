import 'package:HmmAndroidFront/Screens/register.dart';
import 'package:HmmAndroidFront/Screens/verifyotp.dart';
import 'package:HmmAndroidFront/components/sendotpbackground.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';

class SendOTP extends StatefulWidget {
  @override
  _SendOTPState createState() => _SendOTPState();
}

class _SendOTPState extends State<SendOTP> {

  bool _isbuttonenabled = false;
  String phnumber;

  TextEditingController phonenumbercontroller = new TextEditingController();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    _isbuttonenabled = false;
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    phonenumbercontroller.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: SendOTPBackground(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Text(
                  "VERIFICATION",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 30),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(height: size.height * 0.04),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 40),
                child: makeInput(label: "Enter Your Phone Number"),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: RaisedButton(
                  onPressed: _isbuttonenabled
                      ? () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VerifyOTP(phnumber : phonenumbercontroller.text )),
                          )
                      : null,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    width: size.width * 0.5,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: _isbuttonenabled ? Colors.black : Colors.grey[200]),
                    padding: const EdgeInsets.all(0),
                    child: Text(
                      "Send OTP",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: _isbuttonenabled
                              ? Color.fromARGB(255, 255, 208, 0)
                              : Colors.grey,
                          fontSize: 20),
                    ),
                  ),
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }

  Widget makeInput({label, obscureText = false}) {
    return Form(
      key: formkey,
      onChanged: () {
        setState(() {
          _isbuttonenabled = formkey.currentState.validate();
        });
      },
      autovalidate: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: phonenumbercontroller,
            validator: MultiValidator([
              RequiredValidator(errorText: "Required ***"),
              MinLengthValidator(10,
                  errorText: 'Enter 10 Digit Mobile Number ***'),
              MaxLengthValidator(10,
                  errorText: 'Mobile Number cannot exceed 10 digits ***'),
            ]),
            keyboardType: TextInputType.number,
            obscureText: obscureText,
            maxLength: 10,
            decoration: InputDecoration(
              prefixText: '+91',
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  'assets/images/flag.png',
                  height: 0.5,
                  width: 0.5,
                ),
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[400])),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey[400])),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
