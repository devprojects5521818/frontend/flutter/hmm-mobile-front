import 'package:flutter/material.dart';

class AlertDialogs {

  onSuccess(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                  color: Colors.white),
              height: size.shortestSide * 0.70,
              width: size.shortestSide,
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                      color: Colors.green,
                    ),
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: Center(
                      child: Icon(
                        Icons.check_circle,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0),
                              topRight: Radius.circular(25.0),
                              bottomLeft: Radius.circular(25.0),
                              topLeft: Radius.circular(25.0)),
                          color: Colors.white),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, left: 16),
                                child: Text("Success",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                              ),
                              IconButton(
                                iconSize: 30,
                                color: Colors.red,
                                padding: EdgeInsets.only(top: 12,right: 10.0),
                                alignment: Alignment.topRight,
                                icon: Icon(Icons.close),
                                onPressed: () {
                                  Navigator.pop(context, true);
                                },
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            indent: 2,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 16, left: 16),
                            child: Text("The Transaction was Successful",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                          ),
                          Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: SizedBox(
                              child: FlatButton(
                                  minWidth: size.shortestSide * 0.6,
                                  color: Colors.black,
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color:
                                            Color.fromARGB(255, 255, 208, 0)),
                                  ),
                                  onPressed: () {
                                    onWarn(context);
                                    //Navigator.pop(context, true);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  onWarn(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                  color: Colors.white),
              height: size.shortestSide * 0.70,
              width: size.shortestSide,
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                      color: Colors.amber,
                    ),
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: Center(
                      child: Icon(
                        Icons.warning,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0),
                              topRight: Radius.circular(25.0),
                              bottomLeft: Radius.circular(25.0),
                              topLeft: Radius.circular(25.0)),
                          color: Colors.white),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, left: 16),
                                child: Text("Warn",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                              ),
                              IconButton(
                                iconSize: 30,
                                color: Colors.red,
                                padding: EdgeInsets.only(top: 12,right: 10.0),
                                alignment: Alignment.topRight,
                                icon: Icon(Icons.close),
                                onPressed: () {
                                  Navigator.pop(context, true);
                                },
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            indent: 2,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 16, left: 16),
                            child: Text("Some Warning Occured, Please Check",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                          ),
                          Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: SizedBox(
                              child: FlatButton(
                                  minWidth: size.shortestSide * 0.6,
                                  color: Colors.black,
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color:
                                            Color.fromARGB(255, 255, 208, 0)),
                                  ),
                                  onPressed: () {
                                    onInfo(context);
                                    //Navigator.pop(context, true);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  onInfo(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                  color: Colors.white),
              height: size.shortestSide * 0.70,
              width: size.shortestSide,
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                      color: Colors.blueAccent,
                    ),
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: Center(
                      child: Icon(
                        Icons.info_outline_rounded,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0),
                              topRight: Radius.circular(25.0),
                              bottomLeft: Radius.circular(25.0),
                              topLeft: Radius.circular(25.0)),
                          color: Colors.white),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, left: 16),

                                child: Text("Info",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                              ),
                              IconButton(
                                iconSize: 30,
                                color: Colors.red,
                                padding: EdgeInsets.only(top: 12,right: 10.0),
                                alignment: Alignment.topRight,
                                icon: Icon(Icons.close),
                                onPressed: () {
                                  Navigator.pop(context, true);
                                },
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            indent: 2,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 16, left: 16),
                            child: Text("This is a Dummy Information",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                          ),
                          Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: SizedBox(
                              child: FlatButton(
                                  minWidth: size.shortestSide * 0.6,
                                  color: Colors.black,
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color:
                                            Color.fromARGB(255, 255, 208, 0)),
                                  ),
                                  onPressed: () {
                                    onError(context);
                                    //Navigator.pop(context, true);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  onError(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                  color: Colors.white),
              height: size.shortestSide * 0.70,
              width: size.shortestSide,
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                      ),
                      color: Colors.red,
                    ),
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: Center(
                      child: Icon(
                        Icons.error_outline,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(20.0),
                              topRight: Radius.circular(25.0),
                              bottomLeft: Radius.circular(25.0),
                              topLeft: Radius.circular(25.0)),
                          color: Colors.white),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 12, left: 16),

                                child: Text("Error",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                              ),
                              IconButton(
                                iconSize: 30,
                                color: Colors.red,
                                padding: EdgeInsets.only(top: 12,right: 10.0),
                                alignment: Alignment.topRight,
                                icon: Icon(Icons.close),
                                onPressed: () {
                                  Navigator.pop(context, true);
                                },
                              ),
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            indent: 2,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 16, left: 16),
                            child: Text("Some Error Occured ,Please Try Again Later",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                          ),
                          Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: SizedBox(
                              child: FlatButton(
                                  minWidth: size.shortestSide * 0.6,
                                  color: Colors.black,
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color:
                                            Color.fromARGB(255, 255, 208, 0)),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context, true);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  
}
