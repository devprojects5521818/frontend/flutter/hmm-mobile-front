import 'package:flutter/material.dart';

class SendOTPBackground extends StatelessWidget {
  final Widget child;

  const SendOTPBackground({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: size.height,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(
              "assets/images/top3.png",
              width: size.width,
              alignment: Alignment.topRight,
              fit: BoxFit.fitWidth,
              
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: Image.asset(
              "assets/images/top4.png",
              width: size.width,
              alignment: Alignment.topRight,
              fit: BoxFit.fitWidth,
              
            ),
          ),
          Positioned(
            top: 20,
            right: 0.5,
            child: Image.asset(
              "assets/images/otpverify.png",
              width: size.width * 0.50
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Image.asset(
              "assets/images/bottom4.png",
              width: size.width * 1.0,
              alignment: Alignment.bottomLeft,
              fit: BoxFit.fitWidth,
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Image.asset(
              "assets/images/bottom3.png",
              width: size.width,
              alignment: Alignment.bottomLeft,
              fit: BoxFit.fitWidth,
            ),
          ),
          child
        ],
      ),
    );
  }
}